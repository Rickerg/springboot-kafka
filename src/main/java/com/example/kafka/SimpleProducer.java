package com.example.kafka;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;

@Service
public class SimpleProducer {
	
	private KafkaTemplate<String, String> simpleProducer;

	public SimpleProducer(KafkaTemplate<String, String> simpleProducer) {
		this.simpleProducer = simpleProducer;
	}

	public ListenableFuture<SendResult<String, String>> send(String message) {
		return simpleProducer.send("simple-message", message);
	}
}