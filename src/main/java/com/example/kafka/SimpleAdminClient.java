package com.example.kafka;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.stereotype.Component;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;

@Component
public class SimpleAdminClient {

	ListTopicsResult topics ;
	AdminClient client;
	
	public SimpleAdminClient(KafkaProperties kafkaProperties) {

		client =AdminClient.create(kafkaProperties.buildConsumerProperties());
	}
	
	public ListTopicsResult  getTopics() {
	    topics = client.listTopics();
		return topics;
	}
	public CreateTopicsResult createTopic(String topicToAdd) {
		int partitions = 2;
		short replicationFactor = 1;
		List<NewTopic> topicsToAdd = new ArrayList<>();
		NewTopic newTopic = new NewTopic(topicToAdd, partitions, replicationFactor);
		CreateTopicsResult topicResults = client.createTopics(Collections.singleton(newTopic)) ;
				 
		return topicResults;
	}
}
