package com.example.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

@Service
public class SimpleListener {
	@KafkaListener(id = "simple-listener", topics = "simple-message")
	@SendTo("ed_norman")
	public String consumeMessage(String message) {
		System.err.println("Got message from simple_message: " + message);
		return message;
	}
	
	@KafkaListener(id = "ed_norman", topics = "ed_norman")
	public void consumeEdNormansMessage(String message) {
		System.err.println("Got message from ed_norman: " + message);
	}
}