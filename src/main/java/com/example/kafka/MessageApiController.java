package com.example.kafka;


import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.common.KafkaFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MessageApiController {

	@Autowired private SimpleProducer simpleProducer;
	@Autowired private SimpleAdminClient simpleAdminClient;

	@GetMapping("send/{message}")
	public ResponseEntity<String> send(@PathVariable String message) {
		simpleProducer.send(message);
		return ResponseEntity.ok("Message sent: " + message);
	}
	@GetMapping("test/{message}")
	public ResponseEntity<String> sendTest(@PathVariable String message) {
		
		return ResponseEntity.ok("Message sent: " + message);
	}
	
	@GetMapping("listTopics")
	public ResponseEntity<String> getTopics() {
		System.out.println("listTopics");
		StringBuffer results = new StringBuffer();
		ListTopicsResult ltr =simpleAdminClient.getTopics();
		KafkaFuture<Set<String>> names = ltr.names();
		
		try {
			System.out.println("ListTopicsResult" + names.get());
			for( String name : names.get()) {
				results.append("\n");
			    results.append("- " +name);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok("Topics received: " + results.toString());
	}
	@GetMapping("create/{topic}")
	public ResponseEntity<String> create(@PathVariable String topic) {
		
		CreateTopicsResult ctr= simpleAdminClient.createTopic(topic);
		System.out.println("CreateTopicsResult" + ctr.all());
		return ResponseEntity.ok("Message sent: " + ctr);
	}
	@GetMapping("sendtoo/{message}")
	public ResponseEntity<String> sendtoo(@PathVariable String message) {
		ListenableFuture<SendResult<String, String>> future = simpleProducer.send(message);
		future.addCallback( new ListenableFutureCallback<SendResult<String, String>>() {

			@Override
			public void onSuccess(SendResult<String, String> result) {
				System.out.println("onSuccess");
				
			}

			@Override
			public void onFailure(Throwable ex) {
				System.out.println("onFailure");
				
			}
			
		});
		return ResponseEntity.ok("Message sent: " + message);
	}
	
}
