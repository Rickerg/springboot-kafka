package com.example.kafka;

import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;

@Component
public class CustomListener<K, V> extends KafkaMessageListenerContainer<K, V> {

	private static final ContainerProperties containerProps = new ContainerProperties("simple-message", "ed_norman");
	
	static {
		containerProps.setMessageListener(new MessageListener<Integer, String>() {

			@Override
			public void onMessage(ConsumerRecord<Integer, String> data) {
				System.out.println("CustomListener: "+data.value());
			}
		});
	}

	public CustomListener(KafkaProperties kafkaProperties) {
		super(new DefaultKafkaConsumerFactory<>(kafkaProperties.buildConsumerProperties()),containerProps);
		
	}

}
